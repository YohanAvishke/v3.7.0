---
bookCollapseSection: true
weight: 2
---
# About this Release

## What's new in this release

[Entgra IoT Server](https://entgra.io/) version 3.7.0 is the successor of [Entgra IoT Server 3.6.0](https://entgra.atlassian.net/wiki/spaces/IoTS360/overview). Some prominent features and enhancements are as follows:

*   [Device Enrollment Program (DEP) for iOS devices](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/working-with-ios/device-enrollment-program/).

*   Making Entgra IoT Server 3.3.0 General Data Protection Regulations (GDPR) compliant.
    *   [Removing user and device details when the user requests to be forgotten](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/General-Data-Protection-Regulation-for-WSO2-IoT-Server/).

    *   [Consent management when signing in to Entgra IoT Server](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/installation-guide/installing-the-product/).

    *   [Consent management when enrolling devices](https://entgra-documentation.gitlab.io/v3.7.0/docs/tutorials/android/).

    *   Introduction of cookie policies and privacy policies in the device management console.

## Compatible versions

Entgra IoT Server is compatible with WSO2 Carbon 4.4.17 products. For more information on the products in each Carbon platform release, see the [Release Matrix](http://wso2.com/products/carbon/release-matrix/).

## Known issues

For a list of known issues in this release, see [Entgra IoT Server 3.4.0 - Known Issues](https://gitlab.com/entgra/product-iots/).

## Fixed issues

For a list of fixed issues in this release, see [Entgra IoT Server 3.4.0 - Fixed Issues](https://gitlab.com/entgra/product-iots/issues?scope=all&utf8=%E2%9C%93&state=closed).
