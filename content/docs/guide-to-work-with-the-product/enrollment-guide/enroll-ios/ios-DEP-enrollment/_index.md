---
bookCollapseSection: true
weight: 5
---

# iOS DEP Device enrollment(with/without agent)

All you need to do is start up the iOS device that was given to you by your organization and your device will be enrolled with EMM as a DEP device.

If your organization has configured the DEP profile to prompt for your username and password, you need to enter the username and password that is used within the organization.
