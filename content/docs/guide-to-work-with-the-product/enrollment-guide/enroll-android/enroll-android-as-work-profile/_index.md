---
bookCollapseSection: true
weight: 2
---

# Enroll Android as work profile without QR Code


{{< hint info >}}
<strong>Pre-requisites</strong><br>
<ul style="list-style-type:disc;">
    <li>Server is <a href="https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/download-and-start-the-server-/">downloaded and started</a></li>
    <li>Logged into the server's<ahref="https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/login-to-devicemgt-portal/">device mgt portal</a></li>
    <li>Please follow <a href="https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/enrollment-guide/enroll-android/install-agent/"> install agent section </a></li>
    <li>Optionally, basic <a href="https://entgra-documentation.gitlab.io/v3.7.0/docs/key-concepts/#android ">concepts of Android device management</a> will be beneficial as well. </li>
</ul>
{{< /   hint >}}



<iframe width="560" height="315" src="https://www.youtube.com/embed/6hX8ZidTdYw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Open the agent application</li>
    <li>Click continue when disclaimer appears</li>
    <li>In the next screen click "Manual Enrollment"</li>
    <li>In the next screen click "BYOD Enrollment"</li>
    <li>In the next screen click "Setup Work-Profile"</li>
    <li>In the next screen click "Continue"</li>
    <li>Type in the server address which is the IP of the server and port as 8280. Click start registration</li>
    <li>When the policy agreement is shown, click "Agree" to proceed</li>
    <li>Type the Username: admin, Password: admin then click "sign in"</li>
    <li>When the policy agreement is shown, click "Agree" to proceed</li>
    <li>User will be prompted to agree to few permissions and click "Allow"</li>
    <li>Agree to using data usage monitoring to allow server to check the data usage of the device</li>
    <li>Allow agent to change do not disturb status which is used to ring the device</li>
    <li>Enter and confirm a PIN code, which will be needed by admin to perform any critical tasks with user concent. Then click "Set PIN Code" to complete enrollment</li>
</ul>

