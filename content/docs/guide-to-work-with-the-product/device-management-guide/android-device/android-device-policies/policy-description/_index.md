---
bookCollapseSection: true
weight: 1
---

# <strong> Android Device Policy Description </strong>

## <strong> Passcode Policy </strong>

Enforce a configured passcode policy on Android devices. Once this profile is applied, the device 
owners won't be able to modify the password settings on their devices.

<i> Please note that * sign represents required fields of data </i>


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Allow simple value</strong></td>
            <td>Permits repeating, ascending and descending character sequences.
            </td>
        </tr>
        <tr>
            <td><strong>Allow alphanumeric value</strong></td>
            <td>The user must enter a password containing at least both numeric and alphabetic (or other symbol) characters.</td>
        </tr>
        <tr>
            <td><strong>Minimum passcode length</strong></td>
            <td>Set the required number of characters for the password. For example, you can require PIN or passwords to have at least six characters.</td>
        </tr>
        <tr>
            <td><strong>Minimum number of complex characters</strong></td>
            <td>Set the required number of letters, numericals digits, and special symbols that passwords must contain. Introduced in Android 3.0.
        </tr>
        <tr>
            <td><strong>Maximum passcode age in days</strong>
                <br> ( Should be in between 1-to-730 days or 0 for none )</td>
            <td>Designates the full email address for the account. If not present in the payload, the device prompts for this string during profile installation.</td>
        </tr>
        <tr>
            <td><strong>Passcode history</strong>
                <br> ( Should be in between 1-to-50 passcodes or 0 for none )</td>
            <td>Number of consequent unique passcodes to be used before reuse</td>
        </tr>
        <tr>
            <td><strong>Maximum number of failed attempts</strong></td>
            <td>Specifies how many times a user can enter the wrong password before the device wipes its data. The Device Administration API also allows administrators to remotely reset the device to factory defaults. This secures data in case the device is lost or stolen.</td>
        </tr>
    </tbody>
</table>


## <strong> Encryption Settings </strong>
{{< hint info >}}
<b> <a href ="https://entgra-documentation.gitlab.io/v3.8
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This configuration can be used to encrypt data on an Android device, when the device is locked and make it readable when the passcode is entered. Once this configuration profile is installed on a device, corresponding users will not be able to modify these settings on their devices.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Enable storage encryption</strong></td>
            <td>Encryption is the process of encoding all user data on an Android device using symmetric encryption keys.
            Having this checked would enable Storage-encryption in the device.
            </td>
        </tr>     
    </tbody>
</table>

<img src ="encrypt_short.gif" style="border:5px solid black">

{{< hint info >}}
<b> <a href = "https://entgra-documentation.gitlab.io/v3.8
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device
-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}

## <strong> Wi-Fi Settings </strong>

This configurations can be used to configure Wi-Fi access on an Android device. Once this configuration profile is installed on a device, corresponding users will not be able to modify these settings on their devices.



<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Service Set Identifier (SSID)</strong></td>
            <td>The network's SSID. Can either be a UTF-8 string, which must be enclosed in double quotation marks (e.g., "MyNetwork"), or a string of hex digits, which are not enclosed in quotes (e.g., 01a243f405).</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Security</strong>
                    <br>Security type of the wireless network to be configured. 802.1x EAP works with Android 4.3 and above devices only.</center>
            </td>
        </tr>
        <tr>
            <td>
                <strong>WEP</strong>
            </td>
            <td>WEP (Wired Equivalent Privacy) is a security algorithm for IEEE 802.11 wireless networks.
            </td>
        </tr>
        <tr>
            <td><strong>WPA/WPA 2 PSK</strong></td>
            <td>Wi-Fi Protected Access (WPA), Wi-Fi Protected Access II (WPA2) are the security protocols and security certification programs developed by the Wi-Fi Alliance to secure wireless computer networks.</td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td rowspan="6">
                                <center><strong>802.1x EAP</strong></center>
                            </td>
                            <td><strong>EAP Method</strong>
                                <br>EAP is an authentication framework for providing the transport and usage of material and parameters generated by EAP methods .
                            </td>
                            <td>
                                <ul>
                                    <li><strong>PEAP</strong> : PEAP (Protected Extensible Authentication Protocol) is a version of EAP, the authentication protocol used in wireless networks and Point-to-Point connections.</li>
                                    <li><strong>TLS</strong> : EAP uses TLS public key certificate authentication mechanism within EAP to provide mutual authentication of client to server and server to client</li>
                                    <li><strong>TTLS</strong> : The Tunneled TLS EAP method (EAP-TTLS) is very similar to EAP-PEAP in the way that it works and the features that it provides. The difference is that instead of encapsulating EAP messages within TLS, the TLS payload of EAP-TTLS messages consists of a sequence of attributes.</li>
                                    <li><strong>PWD</strong> : EAP-PWD is highly secure (the password is never transmitted, even in encrypted form), and does not require PKI certificates, and also requires only 3 authentication round-trips.</li>
                                    <li><strong>SIM</strong> :</li>
                                    <li><strong>AKA</strong> : The AKA is defined in RFC 5448, and is used for non-3GPP access to a 3GPP core network. For example, via EVDO, WiFi, or WiMax.</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Phase 2 Authentication</strong></td>
                            <td>
                                <ul>
                                    <li><strong>PAP</strong> : Password Authentication Protocol (PAP) is a password-based authentication protocol used by Point to Point Protocol (PPP) to validate users. </li>
                                    <li><strong>MCHAP</strong> : </li>
                                    <li><strong>MCHAPV2</strong> : </li>
                                    <li><strong>GTC</strong> : Generic Token Card (GTC) carries a text challenge from the authentication server, and a reply generated by a security token.</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Identity</strong></td>
                            <td>Identity of the wireless network to be configured.</td>
                        </tr>
                        <tr>
                            <td><strong>Anonymous Identity</strong></td>
                            <td>Identity of the wireless network to be configured.</td>
                        </tr>
                        <tr>
                            <td><strong>CA Certificate</strong></td>
                            <td>CA Certificate for the wireless network.</td>
                        </tr>
                        <tr>
                            <td><strong>Password</strong></td>
                            <td>Password for the wireless network.</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<img src ="encrypt_short.gif" "policy-description/encrypt_short.gif">



## <strong> Global Proxy Settings </strong>

This configurations can be used to set a network-independent global HTTP proxy on an Android device. Once this configuration profile is installed on a device, all the network traffic will be routed through the proxy server.

 <i>This profile requires the agent application to be the device owner.<br>
 This proxy is only a recommendation and it is possible that some apps will ignore it.</i>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Proxy Configuration Type</strong></center>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><i><strong>Manual</strong></i></center>
            </td>
        </tr>
        <tr>
            <td><strong>Proxy Host</strong></td>
            <td>Host name/IP address of the proxy server.
                <br>Eg:[ 192.168.8.1 ]</td>
        </tr>
        <tr>
            <td><strong>Proxy Port</strong></td>
            <td>Target port for the proxy server.
                <br> Eg:[ Target port 0-65535 ]</td>
        </tr>
        <tr>
            <td><strong>Proxy Exclusion List </strong></td>
            <td>Add hostnames to this separated by commas to prevent them from routing through the proxy server. The hostname entries can be wildcards such as *.example.com
                <br>Eg:[ localhost, *.example .com ]</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><i><strong>Auto</strong></i></center>
            </td>
        </tr>
        <tr>
            <td><strong>Proxy PAC File URL </strong></td>
            <td>URL for the proxy auto config PAC script
                <br> Eg: [ http://exampleproxy.com/proxy.pac ]
            </td>
        </tr>
    </tbody>
</table>



## <strong> Virtual Private Network </strong>

### VPN Settings

VPNs allow devices that aren’t physically on a network to securely access the network. Configure 
the OpenVPN settings on Android devices. In order to enable this, device needs to have "OpenVPN for Android" application installed.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>OpenVPN Server Config</strong></td>
           <td> </td>
        </tr>
    </tbody>
</table>

### Always On VPN Settings

Android can start a VPN service when the device boots and keep it running while the device is on. 
This feature is called always-on VPN and is available in Android 7.0 (API Level 24) or higher. 
Configure an always-on VPN connection through a specific VPN client application

<i> Below configurations are valid only when the Agent is work-profile owner or device owner.</i>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>VPN Client Application Package Name</strong></td>
           <td>Package name of the VPN client application to be configured.</td>
        </tr>
    </tbody>
</table>



## <strong> Certificate Install </strong>

This configurations can be used to install certificate on an Android device.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Certificate name</strong></td>
            <td>The file name of the enclosed certificate.
            </td>
        </tr>
        <tr>
            <td><strong>Certificate file</strong></td>
            <td>The base64 representation of the payload with a line length of 52.</td>
        </tr>
        <tr>
            <td><strong>Certificate type</strong></td>
            <td>Certificate should be a DER-encoded X.509 SSL certificate in format of .crt or .cer</td>
        </tr>
    </tbody>
</table>



## <strong> Work-Profile Configurations </strong>

The configurations below can be applied to the devices where the agent is running in Android Work-Profile.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Profile Name</strong></td>
            <td>Name of the Work-Profile created by IoT Server Agent</td>
        </tr>
        <tr>
            <td><strong>Enable System Apps</strong></td>
            <td>Should be exact package names seperated by commas. Ex: com.google.android.apps
            .maps, com.google.android.calculator. The set of system apps needed to be added to the work-profile</td>
        </tr>
        <tr>
            <td><strong>Hide System Apps</strong></td>
            <td>Should be exact package names seperated by commas. Ex: com.google.android.apps
            .maps, com.google.android.calculator.
            The set of system apps needed to be hide in the work-profile</td>
        </tr>
        <tr>
            <td><strong>Unhide System Apps</strong></td>
            <td>Should be exact package names seperated by commas. Ex: com.google.android.apps
            .maps, com.google.android.calculator. The set of system apps needed to be unhide in the work-profile</td>
        </tr>
        <tr>
            <td><strong>Enable Google Play Store Apps</strong></td>
            <td>Should be exact package names seperated by commas. Ex: com.google.android.apps
            .maps, com.google.android.calculator. The set of apps needed to be installed from Google Playstore to work-profile</td>
        </tr>
    </tbody>
</table>



## <strong> COSU Profile Configuration </strong>

This policy can be used to configure the profile of COSU Devices


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Restrict Device Operation 
            Time<br><i>Device will be operable only during the below time period
            .</i></strong></center>
            </td>
        </tr>
            <tr>
                <td><strong>Start Time</strong></td>
                <td>Start time for the device</td>
            </tr>
            <tr>
                <td><strong>End Time</strong></td>
                <td>Lock down time for the device</td>
            </tr>
        <tr>
            <td colspan="2">
                <center><strong>Device Global 
            Configuration</strong>
                    <br>Theme can be configured with the following options.
                </center>
            </td>
        </tr>
            <tr>
                <td><strong>Launcher background image</strong></td>
                <td>This is the image that will be displayed in kiosk background.
                    <br>[ Should be a valid URL of jpg or jpeg or png ]</td>
            </tr>
            <tr>
                <td><strong>Company logo to display</strong></td>
                <td>Company logo to display in the kiosk app drower.
                    <br>[ Should be a valid URL ending with .jpg, .png, .jpeg ]</td>
            </tr>
            <tr>
                <td><strong>Company name</strong></td>
                <td>Name of the company that have to appear on the agent.</td>
            </tr>
        <tr>
            <td>
                <center><strong>Is single application mode</strong></center></td>
            <td>Selected initial app in <strong>Enrollment Application Install policy 
            config</strong> will be selected for single application mode. Atleast one application should be selected. If more than one application is beeing selected, then first selected application in the list will be installed as the single application mode.
                    <ul style="list-style-type:disc;">
                        <li><strong>Is application built for 
            Kiosk</strong> : Is single mode app built for Kiosk. Enable if lock task method is called in the application</li>
                    </ul>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Is idle media enabled</strong>
                    <br>Configuring media to display while idle
                </center>
            </td>
        </tr>
            <tr>
                <td><strong>Media to display while idle</strong></td>
                <td>Url of the media to display while the device is idle.[ Should be a valid URL ending with .jpg, .png, .jpeg, .mp4, .3gp, .wmv, .mkv ]</td>
            </tr>
            <tr>
                <td><strong>Idle graphic begin after(seconds)</strong></td>
                <td>Idle graphic begin after the defined seconds[ Idle timeout should be defined in seconds ]</td>
            </tr>
        <tr>
            <td colspan="2">
                <center><strong>Add User Apps</strong></center>
            </td>
        </tr>
            <tr>
                <td><strong>User</strong></td>
                <td>Specific user name</td>
            </tr>
            <tr>
                <td><strong>Applications</strong></td>
                <td>The applications that are displayed when login using specific user name.</td>
            </tr>
            <tr>
                <td><strong>Device display orientation</strong></td>
                <td>The display orientation of device can be set in a fixed mode.
                    <br>
                    <ul>
                        <li>Auto</li>
                        <li>Potrait</li>
                        <li>Landscape</li>
                    </ul>
                </td>
            </tr>
        <tr>
            <td>
                <center><strong>Is single application mode</strong></center></td>
            <td>Selected initial app in <strong>Enrollment Application Install policy 
            config</strong> will be selected for single application mode. Atleast one application should be selected. If more than one application is beeing selected, then first selected application in the list will be installed as the single application mode.
                    <ul style="list-style-type:disc;">
                        <li><strong>Is application built for 
            Kiosk</strong> : Is single mode app built for Kiosk. Enable if lock task method is called in the application</li>
                    </ul>
            </td>
        </tr>
    </tbody>
</table>

<strong>Is multi-user device</strong>
<br>If Is multi-user device enabled, multi-user configuration can be done for one device. Which 
enables to register already installed applications for registered users. After the policy is applied
these applications can only be executed by logging in as the registered user. Other than this common 
applications which are common to all the users also can be specified by this policy.
<table style="width: 100%;">
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
            <tr>
                <td><strong>Is login needed for user switch</strong></td>
                <td>If this is enabled, the user should have valid user name and password to login to the device.</td>
            </tr>
            <tr>
                <td><strong>Primary User Apps</strong></td>
                <td>Primary User is the user to which the device is enrolled. The applications that are specified in here will be available by default. These applications can be used by any user. Provide comma separated package name or web clip details for applications. eg: com.google.android.apps.maps, {"identity":"http:entgra.io/","title":"entgra-webclip"}</td>
            </tr>
    </tbody>
</table>
<img src ="multi-user.gif" style="border:5px solid black; max-width:63%;" alt="multi user policy gif">
<img src ="multi-user-mobile.gif" style="border:5px solid black; max-width:21%" alt="multi user policy gif">

<strong>Enable Browser Properties</strong>
<br>This can be used to restrict properties of the web browser while using web views.
<table style="width: 100%;">
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
            <tr>
                <td><strong>Primary URL</strong></td>
                <td>Primary URL of the web view</td>
            </tr>
            <tr>
                <td><strong>Enable top control bar</strong></td>
                <td>Enables top control bar that displays all the controllers such as 
                address bar, home button and forwards controllers</td>
            </tr>
            <tr>
                <td><strong>Enable Browser Address Bar</strong></td>
                <td>Enables address bar of the browser.</td>
            </tr>
            <tr>
                <td><strong>Is allow to go back on a page</strong></td>
                <td>Enables to go back in a page</td>
            </tr>
            <tr>
                <td><strong>Is allow to go forward on a page</strong></td>
                <td>Enables to go forward in a page</td>
            </tr>
            <tr>
                <td><strong>Is home button enabled</strong></td>
                <td>Enables browser's home button</td>
            </tr>
            <tr>
                <td><strong>Is page reload enabled</strong></td>
                <td>Enables page reload</td>
            </tr>
            <tr>
                <td><strong>Only allowed to visit the primary url</strong></td>
                <td>Enables visiting URLs other than the primary url</td>
            </tr>
            <tr>
                <td><strong>Is javascript enabled</strong></td>
                <td>Enables loading of javascript from the browser</td>
            </tr>
            <tr>
                <td><strong>Is copying text from browser enabled</strong></td>
                <td>Enables copying texts in the browser</td>
            </tr>
            <tr>
                <td><strong>Is downloading files enabled</strong></td>
                <td>Enable downloading files from the browser</td>
            </tr>
            <tr>
                <td><strong>Is Kiosk limited to one webapp</strong></td>
                <td>Sets whether device can access single or multiple web views</td>
            </tr>
            <tr>
                <td><strong>Is form auto-fill enabled</strong></td>
                <td>Enables autofill to forms in the browser</td>
            </tr>
            <tr>
                <td><strong>Is content access enabled</strong></td>
                <td>Enables content URL access within WebView. Content URL access allows
                 WebView to load content from a content provider installed in the system.</td>
            </tr>
            <tr>
                <td><strong>Is file access enabled</strong></td>
                <td>Sets whether JavaScript running in the context of a file scheme URL should be 
                allowed to access content from other file scheme URLs. .</td>
            </tr>
            <tr>
                <td><strong>Is allowed universal access from file URLs</strong></td>
                <td>Sets whether JavaScript running in the context of a file scheme URL should be 
                allowed to access content from any origin.</td>
            </tr>
            <tr>
                <td><strong>Is application cache enabled.</strong></td>
                <td>Enables web view's application cache</td>
            </tr>
            <tr>
                <td><strong>Application cache file path</strong></td>
                <td>Sets the path to the Application Caches files. In order for the Application
                 Caches API to be enabled, this method must be called with a path to which the application can write</td>
            </tr>
            <tr>
                <td><strong>Application cache mode</strong></td>
                <td>Overrides the way the cache is used. The way the cache is used is based on the
                 navigation type. For a normal page load, the cache is checked and content is re-validated
                 as needed. When navigating back, content is not re-validated, instead the content is just
                 retrieved from the cache. This method allows the client to override this behavior
                 by specifying one of LOAD_DEFAULT, LOAD_CACHE_ELSE_NETWORK, LOAD_NO_CACHE or LOAD_CACHE_ONLY</td>
            </tr>
            <tr>
                <td><strong>Should load images</strong></td>
                <td>Sets whether the browser should load image resources(through network and cached). 
                Note that this method controls loading of all images, including those embedded using the data URI scheme.</td>
            </tr>
            <tr>
                <td><strong>Block image loads via network</strong></td>
                <td>Sets whether the browser should not load image resources from the network (resources accessed via http and https URI schemes)</td>
            </tr>
            <tr>
                <td><strong>Block all resource loads from network</strong></td>
                <td>title="Sets whether the browser should not load any resources from the network."</td>
            </tr>
            <tr>
                <td><strong>Support zooming</strong></td>
                <td>Sets whether the browser should support zooming using its on-screen zoom controls and gestures.</td>
            </tr>
            <tr>
                <td><strong>Show on-screen zoom controllers</strong></td>
                <td>Sets whether the browser should display on-screen zoom controls. Gesture based controllers are still available</td>
            </tr>
            <tr>
                <td><strong>Text zoom percentage</strong></td>
                <td>Sets the text zoom of the page in percent(Should be a positive number)</td>
            </tr>
            <tr>
                <td><strong>Default font size</strong></td>
                <td>Sets the default font size of the browser(Should be a positive number between 1 and 72)</td>
            </tr>
            <tr>
                <td><strong>Default text encoding name</strong></td>
                <td>Sets the default text encoding name to use when decoding html pages(Should a valid text encoding)</td>
            </tr>
            <tr>
                <td><strong>Is database storage API enabled</strong></td>
                <td>Sets whether the database storage API is enabled.</td>
            </tr>
            <tr>
                <td><strong>Is DOM storage API enabled</strong></td>
                <td>Sets whether the DOM storage API is enabled</td>
            </tr>
            <tr>
                <td><strong>Is Geolocation enabled</strong></td>
                <td>Sets whether Geolocation API is enabled</td>
            </tr>
            <tr>
                <td><strong>Can JavaScript open windows</strong></td>
                <td>JavaScript can open windows automatically or not. This applies to the JavaScript function window.open()</td>
            </tr>
            <tr>
                <td><strong>Does media playback requires user consent</strong></td>
                <td>Sets whether the browser requires a user gesture to play media. If false, the browser can play media without user consent</td>
            </tr>
            <tr>
                <td><strong>Is safe browsing enabled</strong></td>
                <td>Sets whether safe browsing is enabled. Safe browsing allows browser to protect against malware and phishing attacks by verifying the links</td>
            </tr>
            <tr>
                <td><strong>Use wide view port</strong></td>
                <td>Sets whether the browser should enable support for the viewport HTML meta tag or should use a wide viewport. 
                When the value of the setting is false, the layout width is always set to the width of the browser control in 
                device-independent (CSS) pixels. When the value is true and the page contains the viewport meta tag, the value of 
                the width specified in the tag is used. If the page does not contain the tag or does not provide a width, 
                then a wide viewport will be used</td>
            </tr>
            <tr>
                <td><strong>Browser user agent string</strong></td>
                <td>Sets the WebView's user-agent string(Should be a valid user agent string)</td>
            </tr>
           <tr>
               <td><strong>Mixed content mode</strong></td>
               <td>Configures the browser's behavior when a secure origin attempts to load a resource from an insecure origin.</td>
           </tr> 
    </tbody>
</table>
<img src ="browser-restriction.gif" style="border:5px solid black; max-width:63%" alt="browser restriction policy gif">



## <strong> Application Restriction Settings </strong>

This configuration can be used to create a black list or white list of applications.

Application blacklisting, is a network administration practice used to prevent the execution of undesirable programs.  Such programs include not only those known to contain security threats or vulnerabilities but also those that are deemed inappropriate within a given organization.

Application whitelisting is the practice of specifying an index of approved software applications or executable files that are permitted to be present and active on a computer system. The goal of whitelisting is to protect computers and networks from potentially harmful applications.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Select type</strong></td>
            <td>Select the type of restriction to proceed.</td>
        </tr>
        <tr>
            <td colspan = "2"><strong><center>Restricted Application List</center></strong></td>
        </tr>
        <tr>
            <td><strong>Application Name/Description</strong></td>
            <td>Eg: [ Gmail ]</td>
        </tr>
        <tr>
            <td><strong>Package Name</strong></td>
            <td>Eg: [ com.google.android.gm ]</td>
        </tr>
    </tbody>
</table>



## <strong> Runtime Permission Policy (COSU / Work Profile) </strong>

This configuration can be used to set a runtime permission policy to an Android Device.

 <i>Already granted or denied permissions are not affected by this policy.
 Permissions can be granted or revoked only for applications built with a Target SDK Version of 
 Android Marshmallow or later.</i>
 
 
<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Set default runtime permission</strong></td>
            <td>When an app requests a runtime permission, this enforces whether the user needs 
            to prompted or the permission (PROMPT USER) either automatically granted (AUTO GRANT)
             or denied (AUTO DENY)
            .</td>
        </tr>
        <tr>
            <td colspan="2"><center><strong>Set app-specific runtime 
            permissions</strong></ceter></td>
        </tr>
        <tr>
            <td><strong>Application</strong></td>
            <td>Eg: [ Android Pay ]</td>
        </tr>
        <tr>
            <td><strong>Package Name</strong></td>
            <td>Eg: [ com.google.android.pay ]</td>
        </tr>
        <tr>
            <td><strong>Permission Name</strong></td>
            <td>Eg: [ android.permission.NFC ]</td>
        </tr>
        <tr>
            <td><strong>Permission Type</strong></td>
            <td>
                <ul>
                    <li>PROMPT USER</li>
                    <li>AUTO GRANT</li>
                    <li>AUTO DENY</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>


## <strong> System Update Policy (COSU) </strong>

This configuration can be used to set a passcode policy to an Android Device. Once this configuration profile is installed on a device, corresponding users will not be able to modify these settings on their devices.
 
<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>System Update</strong>
                    <br>Type of the System Update to be set by the Device Owner
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ul style="list-style-type:disc;">
                    <strong><li>Automatic</li></strong>
                    <strong><li>Postpone</li></strong>
                    <li><strong>Window</strong>
                        <ul>
                            <li><i>Below configuration of start time and end time are 
                                     valid only when window option is selected.</i>
                                <li><strong>Start Time</strong> : Window start time for system update</li>
                                <li><strong>End Time</strong> : Window end time for system update</li>
                        </ul>
                        </li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>




## <strong> Enrollment Application Install </strong>

Enforce applications to be installed during Android device enrollment.

<i>This configuration will be applied only during Android device enrollment.</i>


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Auto Install</strong></td>
            <td>When auto install is checked, then the applications that are selected will be installed autmatically.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Work profile global user 
            configurations</strong></center>
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td rowspan="5">
                                <center><strong>App Auto Update Policy</strong></center>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>When connected to wi-fi</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Auto Update any time</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Ask user to Update</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Disable Auto Update</strong></td>
                        </tr>
                        <tr>
                            <td rowspan="5">
                                <center><strong>App Availability To A User</strong></center>
                            </td>
                            <tr>
                                <tr>
                                    <td><strong>All Approved Apps for Enterprise</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>All Apps from Playstores</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>Only White-listed Apps</strong></td>
                                </tr>
                    </tbody>
                </table>
            </td>
            </tr>
            <tr>
                <td colspan="2">
                    <center><strong>App Install Policy(Work profile only)</strong></center>
                </td>
            </tr>
            <tr>
                <td><strong>App Initial Install Mode</strong>
                    <br>The auto install mode for the first time</td>
                <td>
                    <ul style="list-style-type:disc;">
                        <li>
                            Auto install once only when enrolling
                        </li>
                        <li>Do not install automatically</li>
                        <li>Auto install even if uninstalled manually</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td><strong>Priority level when installing the app</strong>
                    <br>Priority level when installing the app among many other apps</td>
                <td>
                    Lowest - Highest
                </td>
            </tr>
            <tr>
                <td><strong>
                                    Device charging state when installing apps
                                    </strong>
                    <br>Device charging state when installing apps</td>
                <td>
                    <ul style="list-style-type:disc;">
                        <li>Device must be charging</li>
                        <li>Device does not need to be charging</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td><strong>Device processor state when installing</strong>
                    <brDevice processor state when installing</td>
                        <td>
                            <ul style="list-style-type:disc;">
                                <li>
                                    Device does not need to be idling
                                </li>
                                <li>Device must be idling</li>
                            </ul>
                        </td>
            </tr>
            <tr>
                <td><strong>Device network state when installing</strong>
                    <br>Device processor state when installing</td>
                <td>
                    <ul style="list-style-type:disc;">
                        <li>
                            Device can be in any network
                        </li>
                        <li>Device must be in an unmetered network</li>
                    </ul>
                </td>
            </tr>
    </tbody>
</table>

