---
bookCollapseSection: true
weight: 4
---

# Certificate Management


Entgra IoT Server supports mutual SSL authentication to ensure a trusted relationship between the server and the client. 


## Mutual SSL Authentication


In contrast to the usual one-way SSL authentication where a client verifies the identity of the server, in mutual SSL the server validates the identity of the client so that both parties trust each other. This builds a system that has a very tight security and avoids any requests made to the client to provide the username/password, as long as the server is aware of the certificates that belong to the client.

Before the process begins the client and servers certificates are stored in there relevant `keystores`. In the case of JAVA they are `jks` files. Let's take a look at where the JKS files are saved:

*   Entgra product certificates are stored in the `wso2carbon.jks` file.
*   Server side certificates are stored in the `clienttruststore.jks` file. 
*   Android agent side certificates are stored in the `bouncycastle keystores(bks)` files that are in the `<ANDROID_AGENT_SOURCE_HOME>/client/iDPProxy/src/main/res/raw` directory in the [Android agent source code.](https://github.com/wso2/cdmf-agent-android/releases/tag/v2.0.0)  

These certificates are signed and issued by a certificate authority that allows both the client and server to communicate freely. Now let's look at how it works:

![image](352821123.png)

1.  The Client attempts to access a protected resource and the SSL/TSL handshake process begins.
2.  The Server presents its certificate, which is the `server.crt` according to our example as shown above. 
3.  The Client takes this certificate and asks the certificate issued authority for the authenticity and validity of the certificate.
4.  If the certificate is valid, the client will also provide its certificate to the server.
5.  The Server takes this certificate and asks the certificate issued authority for the authenticity and validity of the certificate.
6.  The Client is granted access to the resource it was trying to access earlier. 





For more information adding certificates via the device management console, see [Managing Client Side Mutual SSL Certificates](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/certificate-management/#mutual-ssl-authentication).



## Managing Client Side Mutual SSL Certificates


Entgra IoT Server supports mutual SSL, where the client verifies that the server can be trusted and the server verifies that the client can be trusted by using digital signatures. The following sections illustrate how to manage the client-side mutual SSL certificates.

This section guides you through adding, deleting, searching, and sorting certificates in Entgra IoT Server.  

### Adding certificates

Follow the instructions given below to add certificates:

1.  [Sign in to the Entgra IoT Server device management console](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-entgra-iot-server-management-console) and click the
![image](1.png) icon.

2.  Click **Configuration Management **> **Certificate Configurations**.  
    ![image](352821146.png)
3.  Click **ADD CERTIFICATE**,and Upload the SSL certificate.  
    ![image](352821136.png)
4.  Click **Add Certificate **to finish adding the certificate.  
    Once the certificate is added successfully click **View Certificate List** to view the certificates you have uploaded or click **Add Another Certificate** to upload another SSL certificate. 

### Removing certificates

Follow the instructions given below to delete a certificate: 

1.  Click on the menu icon, and then click **Configuration Management**.
2.  Click **Certificate Configuration**. You will see a list of all the certificates that have been uploaded as shown below.   
    ![image](352821156.png)
3.  Click on the ![image](3.png) icon to delete a certificate.

    

    

    If you are unable to find your certificate on the list, you can first search for the certificate by its serial number, and then delete it.

    

    

4.  Click **REMOVE** to confirm that you wish to delete the certificate.  
    ![image](352821176.png)

### Searching certificates

Follow the instructions given below to search for a particular certificate: 

1.  Click on the menu icon and then click **Configuration Management**.
2.  Click **Certificate Configuration**. You will see a list of all the certificates that have been uploaded as shown below.   
    ![image](352821156.png)
3.  Enter the full serial number of the certificate you want to search for or part of it, and click **Search**.  
    ![image](352821166.png)

### Sorting certificates

Follow the instructions given below to sort the certificates: 

1.  Click on the menu icon, and then click **Configuration Management**.
2.  Click **Certificate Configuration**.
3.  Hover over the sort icon found on the top right corner of the screen, and click **By Serial Number**.  
    ![image](352821161.png)
