# Clusturing Entgra IoT server


## Clusturing entgra IoT server
The following diagram illustrates a typical deployment pattern for Entgra IoT Server.
![image](352819863.png)


As indicated in the above diagram, when clustering Entgra IoT Server, there is worker manager separation. In a standard Entgra product cluster, worker and manager separation is derived from deployment synchronization. However, this differs from standard WSO2 Carbon worker manager separation.

Entgra IoT Server includes an admin console that can be used by any user with administrative privileges. These users can perform operations on enrolled devices and the devices can retrieve those actions by requesting for the pending operations. This is done by either walking the device through a push notification or configuring the device to poll at a pre-configured frequency.

Normally administrative tasks should be run from a manager node.

There are two major deployment patterns for the manager node. One could be running the manager node in the private network due to security constraints and other is allowing end users to access the management node so that they can control and view their devices.

A manager node is used to run background tasks that are necessary to update device information such as location and the list of installed applications. For more information on creating different profiles in Entgra IoT Server, see [Product Profiles](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/product-profiles/).

Let's take a look at the steps to cluster Entgra IoT Server:

*   **[Configuring the Load Balancer](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/clusturing-entgra-iot-server/#configuring-the-load-balancer)**
*   **[Setting Up the Databases for Clustering](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/clusturing-entgra-iot-server/#setting-up-the-databases-for-clustering)**
*   **[Mounting the Registry](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/clusturing-entgra-iot-server/#mounting-the-registry)**
*   **[Configuring the Key Manager Node](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/clusturing-entgra-iot-server/#configuring-the-key-manager-node)**
*   **[Configuring the Manager Node](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/clusturing-entgra-iot-server/#configuring-the-manager-node)**
*   **[Configuring the Worker Nodes](/doc/en/lb2/Configuring-the-Worker-Nodes.html)**
*   **[Clustering the iOS Server](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/clusturing-entgra-iot-server/#clustering-the-ios-server)**
*   **[Clustering WSO2 DAS for Analytics](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/clusturing-entgra-iot-server/#clustering-wso2-das-for-analytics)**



Before you begin



You need the following to cluster Entgra IoT Server:

## Virtual machines used in a high availability cluster

The following is a list of virtual machines (VMs) that are used in a high availability cluster and their details.

*   Manager - 1 VM
*   Worker - 2 VMs
*   Key manager - 2 VMs
*   DBs - 1 MySQL instance

All the VMs have 4 cores and 4GB memory.

## Open ports

80 and 443 are from the NGINX server.

The following ports need to be opened for Android and iOS devices so that it can connect to GCM (Google Cloud Message) and APNS (Apple Push Notification Service) and enroll to Entgra IoT Server.

**Android**

The ports to open are 5228, 5229 and 5230\. GCM typically uses only 5228, but it sometimes uses 5229 and 5230.

GCM does not provide specific IPs, so it is recommended to allow the firewall to accept outgoing connections to all IP addresses contained in the IP blocks listed in Google's ASN of 15169\.  

**iOS**

*   5223 - TCP port used by devices to communicate to APNS servers

*   2195 - TCP port used to send notifications to APNS

*   2196 - TCP port used by the APNS feedback service

*   443 - TCP port used as a fallback on Wi-Fi, only when devices are unable to communicate to APNS on port 5223

The APNS servers use load balancing. The devices will not always connect to the same public IP address for notifications. The entire 17.0.0.0/8 address block is assigned to Apple, so it is best to allow this range in the firewall settings.


# Configuring the load balancer

This section provides instructions on how to configure Nginx as the load balancer. You can use any load balancer for your setup.

The location of the file varies depending on how you installed the software on your machine. For many distributions, the file is located at `/etc/nginx/nginx.conf`. If it does not exist there, it may also be at `/usr/local/nginx/conf/nginx.conf` or `/usr/local/etc/nginx/nginx.conf`. You can create separate files inside the `conf.d` directory for each configuration. Three different configuration files are used for the Manager, Key Manager and Worker nodes in the example provided in this page.

Before you begin


You need to have a signed SSL certificate before starting. When generating the certificate make sure to add the following four URLs as Server Name Indications (SNI).

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th rowspan="2">Worker</th>
      <td>iots310.wso2.com</td>
    </tr>
    <tr>
      <td>mgt.iots310.wso2.com</td>
    </tr>
    <tr>
      <th>Manager</th>
      <td>keymgt.iots310.wso2.com</td>
    </tr>
    <tr>
      <th>Key Manager</th>
      <td>Gateway.iots310.wso2.com</td>
    </tr>
  </tbody>
</table>


1.  Create a file named `mgt.conf` in the /`nginx/conf.d` directory and add the following to it. This will be used by the Manager node to load balance.



    upstream mgt.iots310.wso2.com {
            ip_hash;
            server 192.168.57.124:9763;
    }

    server {
            listen 80;
            server_name mgt.iots310.wso2.com;
            client_max_body_size 100M;
            location / {
                   proxy_set_header X-Forwarded-Host $host;
                   proxy_set_header X-Forwarded-Server $host;
                   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                   proxy_set_header Host $http_host;
                   proxy_read_timeout 5m;
                   proxy_send_timeout 5m;
                   proxy_pass http://mgt.iots310.wso2.com;

                   proxy_http_version 1.1;
                   proxy_set_header Upgrade $http_upgrade;
                   proxy_set_header Connection "upgrade";
            }
    }

    upstream ssl.mgt.iots310.wso2.com {
        ip_hash;
        server 192.168.57.124:9443;

    }

    server {
    listen 443;
        server_name mgt.iots310.wso2.com;
        ssl on;
        ssl_certificate /opt/keys/star_wso2_com.crt;
        ssl_certificate_key /opt/keys/iots310_wso2_com.key;
     client_max_body_size 100M;
        location / {
                   proxy_set_header X-Forwarded-Host $host;
                   proxy_set_header X-Forwarded-Server $host;
                   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                   proxy_set_header Host $http_host;
                   proxy_read_timeout 5m;
                   proxy_send_timeout 5m;
                   proxy_pass https://ssl.mgt.iots310.wso2.com;

                   proxy_http_version 1.1;
                   proxy_set_header Upgrade $http_upgrade;
                   proxy_set_header Connection "upgrade";
            }
    }
    

2.  Create a file named `wkr.conf` in the /`nginx/conf.d` directory and add the following to it. This will be used by the first worker node to load balance.

    
    upstream iots310.wso2.com {
            ip_hash;
            server 192.168.57.125:9763;
            server 192.168.57.126:9763;
    }

    server {
            listen 80;
            server_name iots310.wso2.com;
            location / {
                   proxy_set_header X-Forwarded-Host $host;
                   proxy_set_header X-Forwarded-Server $host;
                   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                   proxy_set_header Host $http_host;
                   proxy_read_timeout 5m;
                   proxy_send_timeout 5m;
                   proxy_pass http://iots310.wso2.com;

                   proxy_http_version 1.1;
                   proxy_set_header Upgrade $http_upgrade;
                   proxy_set_header Connection "upgrade";
            }
    }

    upstream ssl.iots310.wso2.com {
        ip_hash;
        server 192.168.57.125:9443;
        server 192.168.57.126:9443;
    }

    server {
    listen 443;
        server_name iots310.wso2.com;
        ssl on;
        ssl_certificate /opt/keys/star_wso2_com.crt;
        ssl_certificate_key /opt/keys/iots310_wso2_com.key;
        location / {
                   proxy_set_header X-Forwarded-Host $host;
                   proxy_set_header X-Forwarded-Server $host;
                   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                   proxy_set_header Host $http_host;
                   proxy_read_timeout 5m;
                   proxy_send_timeout 5m;
                   proxy_pass https://ssl.iots310.wso2.com;

                   proxy_http_version 1.1;
                   proxy_set_header Upgrade $http_upgrade;
                   proxy_set_header Connection "upgrade";
            }
    }
    

3.  Create a file named `gateway.conf` in the /`nginx/conf.d` directory and add the following to it. This will be used by the gateway worker node to load balance.

    upstream gateway.iots310.wso2.com {
            ip_hash;
            server 192.168.57.125:8280;
            server 192.168.57.126:8280;
    }

    server {
            listen 80;
            server_name gateway.iots310.wso2.com;
            location / {
                   proxy_set_header X-Forwarded-Host $host;
                   proxy_set_header X-Forwarded-Server $host;
                   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                   proxy_set_header Host $http_host;
                   proxy_read_timeout 5m;
                   proxy_send_timeout 5m;
                   proxy_pass http://gateway.iots310.wso2.com;

                   proxy_http_version 1.1;
                   proxy_set_header Upgrade $http_upgrade;
                   proxy_set_header Connection "upgrade";
            }
    }

    upstream ssl.gateway.iots310.wso2.com {
        ip_hash;
        server 192.168.57.125:8243;
        server 192.168.57.126:8243;
    }

    server {
    listen 443;
        server_name gateway.iots310.wso2.com;
        ssl on;
        ssl_certificate /opt/keys/star_wso2_com.crt;
        ssl_certificate_key /opt/keys/iots310_wso2_com.key;
        location / {
                   proxy_set_header X-Forwarded-Host $host;
                   proxy_set_header X-Forwarded-Server $host;
                   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                   proxy_set_header Host $http_host;
                   proxy_read_timeout 5m;
                   proxy_send_timeout 5m;
                   proxy_pass https://ssl.gateway.iots310.wso2.com;

                   proxy_http_version 1.1;
                   proxy_set_header Upgrade $http_upgrade;
                   proxy_set_header Connection "upgrade";
            }
    }
    

4.  Create a file named `keymgt.conf` in the /`nginx/conf.d` directory and add the following to it. This will be used by the key manager node to load balance.

    upstream keymgt.iots310.wso2.com {
            ip_hash;
            server 192.168.57.127:9763;
    }

    server {
            listen 80;
            server_name keymgt.iots310.wso2.com;
            location / {
                   proxy_set_header X-Forwarded-Host $host;
                   proxy_set_header X-Forwarded-Server $host;
                   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                   proxy_set_header Host $http_host;
                   proxy_read_timeout 5m;
                   proxy_send_timeout 5m;
                   proxy_pass http://keymgt.iots310.wso2.com;

                   proxy_http_version 1.1;
                   proxy_set_header Upgrade $http_upgrade;
                   proxy_set_header Connection "upgrade";
            }
    }

    upstream ssl.keymgt.iots310.wso2.com {
        ip_hash;
        server 192.168.57.127:9443;

    }

    server {
    listen 443;
        server_name keymgt.iots310.wso2.com;
        ssl on;
        ssl_certificate /opt/keys/star_wso2_com.crt;
        ssl_certificate_key /opt/keys/iots310_wso2_com.key;
        location / {
                   proxy_set_header X-Forwarded-Host $host;
                   proxy_set_header X-Forwarded-Server $host;
                   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                   proxy_set_header Host $http_host;
                   proxy_read_timeout 5m;
                   proxy_send_timeout 5m;
                   proxy_pass https://ssl.keymgt.iots310.wso2.com;

                   proxy_http_version 1.1;
                   proxy_set_header Upgrade $http_upgrade;
                   proxy_set_header Connection "upgrade";
            }
    }



# Setting Up the Databases for Clustering


The following databases are needed when clustering Entgra IoT Server.

<table>
  <colgroup>
    <col>
    <col>
    <col>
  </colgroup>
  <thead>
    <tr>
      <th>Database Name</th>
      <th>Description</th>
      <th>Database Script Location</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>APIM Database (WSO2AM_DB)</td>
      <td>This database stores data related to JAX-RS APIs and OAuth token data.</td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/apimgt/</code></td>
    </tr>
    <tr>
      <td>
        <p>App management database<br>(WSO2APPM_DB)</p>
      </td>
      <td>This database store the mobile and IoT device application details.</td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/appmgt</code></td>
    </tr>
    <tr>
      <td>CDM core database (DM_DS)</td>
      <td>This database stores generic data about devices (such as a unique identifier, device type, ownership type), device enrollment information, device operations, policy management related data, etc.</td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/cdm/</code></td>
    </tr>
    <tr>
      <td>Certificate management database</td>
      <td>This database stores the mutual SSL certificate details.</td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/certmgt</code></td>
    </tr>
    <tr>
      <td>Registry database (REG_DB)</td>
      <td>This database acts as the registry database and stores governance and config registry data. The registry database must be mounted to all nodes in the cluster.</td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts<br></code><span>Example: The</span> <code>mysql.sql</code><span> script to create the database for MySQL.</span></td>
    </tr>
    <tr>
      <td>
        <p>Social database</p>
        <p>(WSO2_SOCIAL_DB)</p>
      </td>
      <td>The database used by the WSO2 App Manager Store to gather the likes, comments, and rating of an application.</td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/social</code></td>
    </tr>
    <tr>
      <td>Storage database</td>
      <td>The database is used by the WSO2 App Manager to store web application data, such as URI templates, subscriptions, and more.</td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/storage</code></td>
    </tr>
    <tr>
      <td>User manager database (UM_DB)</td>
      <td>This database stores the user details and user permission related details.</td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts </code><span>Example: The</span> <code><code>mysql.sql</code></code> <span>script to create the database for MySQL.</span><code></code></td>
    </tr>
  </tbody>
</table>

The following databases are related to plugins. These enable you to keep the data that is essential for these devices to work (such as APNS related keys) and this data is not available in the CDM core database.

<table>
  <colgroup>
    <col>
    <col>
    <col>
  </colgroup>
  <thead>
    <tr>
      <th>Database Name</th>
      <th>Description</th>
      <th>Database Script Location</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>iOS database (MobileIOSDM_DS)</td>
      <td>
        <p>Stores the iOS related the data.</p>
        <p>If you have not configured Entgra IoT Server for iOS, you won't have the database scripts in the given location. For more information on configuring Entgra IoT Server for iOS, see&nbsp;<a href="https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/working-with-ios/ios-configurations/" class="external-link" rel="nofollow">iOS Configurations</a>.</p>
      </td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/cdm/plugins/ios</code></td>
    </tr>
    <tr>
      <td>Android database (MobileAndroidDM_DS)</td>
      <td>Stores the Android related data.</td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/cdm/plugins/android</code></td>
    </tr>
    <tr>
      <td>Windows database (MobileWindowsDM_DS)</td>
      <td>Stores the Microsoft Windows related data.</td>
      <td><code>&lt;IOTS_HOME&gt;/dbscripts/cdm/plugins/windows</code></td>
    </tr>
  </tbody>
</table>

To change the datasource configurations, please change the following files.





**NOTE**: Make sure to add the relevant JDBC library to the `<IOTS_HOME>/lib` directory. For example, add the `mysql-connector-java-{version}.jar` file to the `<IOTS_HOME>/lib` directory when using the MySQL database.





<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <thead>
    <tr>
      <th>Files to change HERE</th>
      <th>Datasource</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>&lt;IOTS_HOME&gt;/repository/conf<br>/datasources/master-datasources.xml</code></td>
      <td>
        
          <p>This file must include the datasource configuration for the following databases.</p>
          <ul>
            <li>
              <p>APIM database</p>
              <p>Make sure to use the <code>zeroDateTimeBehavior=convertToNull</code> parameter when configuring the database using MySQL.</p>
                          &lt;datasource&gt;
   &lt;name&gt;WSO2AM_DB&lt;/name&gt;
   &lt;description&gt;The datasource used for API Manager database&lt;/description&gt;
   &lt;jndiConfig&gt;
      &lt;name&gt;jdbc/WSO2AM_DB&lt;/name&gt;
   &lt;/jndiConfig&gt;
   &lt;definition type="RDBMS"&gt;
      &lt;configuration&gt;
         &lt;url&gt;jdbc:mysql://{hostname}:{port}/apim?autoReconnect=true&amp;amp;relaxAutoCommit=true&amp;amp;zeroDateTimeBehavior=convertToNull&lt;/url&gt;
         &lt;username&gt;root&lt;/username&gt;
         &lt;password&gt;root&lt;/password&gt;
         &lt;driverClassName&gt;com.mysql.jdbc.Driver&lt;/driverClassName&gt;
         &lt;maxActive&gt;50&lt;/maxActive&gt;
         &lt;maxWait&gt;60000&lt;/maxWait&gt;
         &lt;testOnBorrow&gt;true&lt;/testOnBorrow&gt;
         &lt;validationQuery&gt;SELECT 1&lt;/validationQuery&gt;
         &lt;validationInterval&gt;30000&lt;/validationInterval&gt;
      &lt;/configuration&gt;
   &lt;/definition&gt;
&lt;/datasource&gt;
            </li>
            <li>
              <p>Registry database</p>
            </li>
            <li>
              <p>User management database</p>
  &lt;datasource&gt;
   &lt;name&gt;WSO2UM_DB&lt;/name&gt;
   &lt;description&gt;The datasource used for User Manager database&lt;/description&gt;
   &lt;jndiConfig&gt;
      &lt;name&gt;jdbc/WSO2UM_DB&lt;/name&gt;
   &lt;/jndiConfig&gt;
   &lt;definition type="RDBMS"&gt;
      &lt;configuration&gt;
         &lt;url&gt;jdbc:mysql://{hostname}:{port}/userdb?autoReconnect=true&amp;amp;relaxAutoCommit=true&lt;/url&gt;
         &lt;username&gt;root&lt;/username&gt;
         &lt;password&gt;root&lt;/password&gt;
         &lt;driverClassName&gt;com.mysql.jdbc.Driver&lt;/driverClassName&gt;
         &lt;maxActive&gt;50&lt;/maxActive&gt;
         &lt;maxWait&gt;60000&lt;/maxWait&gt;
         &lt;testOnBorrow&gt;true&lt;/testOnBorrow&gt;
         &lt;validationQuery&gt;SELECT 1&lt;/validationQuery&gt;
         &lt;validationInterval&gt;30000&lt;/validationInterval&gt;
      &lt;/configuration&gt;
   &lt;/definition&gt;
&lt;/datasource&gt;
            </li>
            <li>App Manager database</li>
            <li>Store database</li>
            <li>Social database</li>
          </ul>
      </td>
    </tr>
    <tr>
      <td><code>&lt;IOTS_HOME&gt;/repository/conf<br>/datasources/cdm-datasources.xml</code></td>
      <td>
      <p>This file must include the datasource configuration for the CDM core database.</p>
 &lt;datasources&gt;
   &lt;datasource&gt;
      &lt;name&gt;DM_DS&lt;/name&gt;
      &lt;description&gt;The datasource used for CDM&lt;/description&gt;
      &lt;jndiConfig&gt;
         &lt;name&gt;jdbc/DM_DS&lt;/name&gt;
      &lt;/jndiConfig&gt;
      &lt;definition type="RDBMS"&gt;
         &lt;configuration&gt;
            &lt;url&gt;jdbc:mysql://{localhost}:3306/cdm?autoReconnect=true&amp;amp;relaxAutoCommit=true&lt;/url&gt;
            &lt;username&gt;root&lt;/username&gt;
            &lt;password&gt;root&lt;/password&gt;
            &lt;driverClassName&gt;com.mysql.jdbc.Driver&lt;/driverClassName&gt;
            &lt;maxActive&gt;50&lt;/maxActive&gt;
            &lt;maxWait&gt;60000&lt;/maxWait&gt;
            &lt;testOnBorrow&gt;true&lt;/testOnBorrow&gt;
            &lt;validationQuery&gt;SELECT 1&lt;/validationQuery&gt;
            &lt;validationInterval&gt;30000&lt;/validationInterval&gt;
         &lt;/configuration&gt;
      &lt;/definition&gt;
   &lt;/datasource&gt;
&lt;/datasources&gt;
      </td>
    </tr>
    <tr>
      <td><code>&lt;IOTS_HOME&gt;/repository/conf<br>/datasources/android-datasources.xml</code></td>
      <td>This file must include the datasource configuration for the Android plugin database.</td>
    </tr>
    <tr>
      <td><code>&lt;IOTS_HOME&gt;/repository/conf<br>/datasources/ios-datasources.xml</code></td>
      <td>This file must include the datasource configuration for the iOS plugin database.</td>
    </tr>
    <tr>
      <td><code>&lt;PRODUCT_HOME&gt;/repository/conf<br>/datasources/windows-datasources.xml</code></td>
      <td>This file must include the datasource configuration for the Windows Plugin database.</td>
    </tr>
  </tbody>
</table>


# Mounting the registry

A registry is a virtual directory based repository system. It can be federated among multiple databases, which is referred to as Registry mounting. All Entgra IoT Server supports registry mounting. There are three types of registry-repositories.

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Local</th>
      <td>Stores the data related to the local instances.</td>
    </tr>
    <tr>
      <th>Config</th>
      <td>Contains product specific configurations that are shared across multiple instances of the same product.</td>
    </tr>
    <tr>
      <th>Governance</th>
      <td>Contains data and configurations that are shared across all the products in the platform.</td>
    </tr>
  </tbody>
</table>

For more information on sharing registry spaces across multiple product instances, see [here](http://wso2.com/library/tutorials/2010/04/sharing-registry-space-across-multiple-product-instances).

See [Remote Instance and Mount Configuration Details ](https://docs.wso2.com/display/Governance510/Remote+Instance+and+Mount+Configuration+Details)for more information on registry mounting and why it is useful. These must be done on all nodes. Follow the steps given below:

1.  Add the following to the `<IOTS_HOME>/conf/` `datasources/master-datasources.xml` file to configure the Key Manager registry mounting.

    <datasource>
                <name>WSO2REG_DB</name>
                <description>The datasource used for registry</description>
                <jndiConfig>
                    <name>jdbc/WSO2REG_DB</name>
                </jndiConfig>
                <definition type="RDBMS">
                    <configuration>
                        <url>jdbc:mysql://{hostname}:{port}/WSO2REG_DB?autoReconnect=true&amp;relaxAutoCommit=true</url>
                        <username>root</username>
                        <password>1234</password>
                        <driverClassName>com.mysql.jdbc.Driver</driverClassName>
                        <maxActive>50</maxActive>
                        <maxWait>60000</maxWait>
                        <testOnBorrow>true</testOnBorrow>
                        <validationQuery>SELECT 1</validationQuery>
                        <validationInterval>30000</validationInterval>
                    </configuration>
                </definition>
            </datasource>
    

2.  Add the following configurations to the `<IOTS_HOME>/conf/registry.xml` file to configure the Worker and Manager registry mounting.

    
    <dbConfig name="mounted_registry">
       <dataSource>jdbc/WSO2REG_DB</dataSource>
    </dbConfig>

    <remoteInstance url="https://localhost:9443/registry">
       <id>instanceid</id>
       <dbConfig>mounted_registry</dbConfig>
       <readOnly>false</readOnly>
       <enableCache>true</enableCache>
       <registryRoot>/</registryRoot>
       <cacheId>root@jdbc:mysql://192.168.57.123:3306/govreg</cacheId>
    </remoteInstance>

    <mount path="/_system/config" overwrite="true">
       <instanceId>instanceid</instanceId>
       <targetPath>/_system/config/iot</targetPath>
    </mount>
    <mount path="/_system/governance" overwrite="true">
       <instanceId>instanceid</instanceId>
       <targetPath>/_system/governance</targetPath>
    </mount>
    
    
# Configuring the Key Manager Node


Through out this guide you have configured `keymgt.iots310.wso2.com` as the key manager node.


Before you begin

*   Mount the registry as explained [here](/doc/en/lb2/Mounting-the-Registry.html).
*   Configure the following databases for the Key Manager in the `<IOTS_HOME>/conf/datasources/master-datasources.xml` file.  
    For more information, see [Setting Up the Databases for Clustering](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/clusturing-entgra-iot-server/#setting-up-the-databases-for-clustering).
    *   Registry Database
    *   User manager database
    *   APIM Database





Let's start configuring the Key Manager node.

1.  Configure the `HostName` and `MgtHostName` properties in the `<IOTS_HOME>/conf/carbon.xml` file as shown below.

    
    <HostName>keymgt.iots310.wso2.com</HostName>
    <MgtHostName>keymgt.iots310.wso2.com</MgtHostName>

    Make sure to have the `Offset` property configured to zero. If it is set to a value other than zero, you need to update the NGINX configuration based on the port offset.


2.  Configure the `<IOTS_HOME>/bin/iot-server.sh` file as shown below:

    
    -Diot.keymanager.host="keymgt.iots310.wso2.com" \
     -Diot.keymanager.https.port="443" \
    

3.  Configure all the following properties in the `<IOTS_HOME>/conf/identity/sso-idp-config.xml` file by replacing `https://localhost:9443` with `https://mgt.iots310.wso2.com:443`.

    *   `AssertionConsumerServiceURL`

    *   `DefaultAssertionConsumerServiceURL` 

    

    

    

    
    <SSOIdentityProviderConfig>
       <TenantRegistrationPage>https://stratos-local.wso2.com/carbon/tenant-register/select_domain.jsp</TenantRegistrationPage>
       <ServiceProviders>
          <ServiceProvider>
             <Issuer>devicemgt</Issuer>
             <AssertionConsumerServiceURLs>
                <AssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/devicemgt/uuf/sso/acs</AssertionConsumerServiceURL>
             </AssertionConsumerServiceURLs>
             <DefaultAssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/devicemgt/uuf/sso/acs</DefaultAssertionConsumerServiceURL>
             <SignAssertion>true</SignAssertion>
             <SignResponse>true</SignResponse>
             <EnableAttributeProfile>false</EnableAttributeProfile>
             <IncludeAttributeByDefault>false</IncludeAttributeByDefault>
             <Claims>
                <Claim>http://wso2.org/claims/role</Claim>
                <Claim>http://wso2.org/claims/emailaddress</Claim>
             </Claims>
             <EnableAudienceRestriction>true</EnableAudienceRestriction>
             <EnableRecipients>true</EnableRecipients>
             <AudiencesList>
                <Audience>https://localhost:9443/oauth2/token</Audience>
             </AudiencesList>
             <RecipientList>
                <Recipient>https://localhost:9443/oauth2/token</Recipient>
             </RecipientList>
          </ServiceProvider>
          <ServiceProvider>
             <Issuer>store</Issuer>
             <AssertionConsumerServiceURLs>
                <AssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/store/acs</AssertionConsumerServiceURL>
             </AssertionConsumerServiceURLs>
             <DefaultAssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/store/acs</DefaultAssertionConsumerServiceURL>
             <SignResponse>true</SignResponse>
             <CustomLoginPage>/store/login.jag</CustomLoginPage>
          </ServiceProvider>
          <ServiceProvider>
             <Issuer>social</Issuer>
             <AssertionConsumerServiceURLs>
                <AssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/social/acs</AssertionConsumerServiceURL>
             </AssertionConsumerServiceURLs>
             <DefaultAssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/social/acs</DefaultAssertionConsumerServiceURL>
             <SignResponse>true</SignResponse>
             <CustomLoginPage>/social/login</CustomLoginPage>
          </ServiceProvider>
          <ServiceProvider>
             <Issuer>publisher</Issuer>
             <AssertionConsumerServiceURLs>
                <AssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/publisher/acs</AssertionConsumerServiceURL>
             </AssertionConsumerServiceURLs>
             <DefaultAssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/publisher/acs</DefaultAssertionConsumerServiceURL>
             <SignResponse>true</SignResponse>
             <CustomLoginPage>/publisher/controllers/login.jag</CustomLoginPage>
             <EnableAudienceRestriction>true</EnableAudienceRestriction>
             <AudiencesList>
                <Audience>carbonServer</Audience>
             </AudiencesList>
          </ServiceProvider>
          <ServiceProvider>
             <Issuer>API_STORE</Issuer>
             <AssertionConsumerServiceURLs>
                <AssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/api-store/jagg/jaggery_acs.jag</AssertionConsumerServiceURL>
             </AssertionConsumerServiceURLs>
             <DefaultAssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/api-store/jagg/jaggery_acs.jag</DefaultAssertionConsumerServiceURL>
             <SignResponse>true</SignResponse>
             <EnableAudienceRestriction>true</EnableAudienceRestriction>
             <AudiencesList>
                <Audience>carbonServer</Audience>
             </AudiencesList>
          </ServiceProvider>
          <ServiceProvider>
             <Issuer>portal</Issuer>
             <AssertionConsumerServiceURLs>
                <AssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/portal/acs</AssertionConsumerServiceURL>
             </AssertionConsumerServiceURLs>
             <DefaultAssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/portal/acs</DefaultAssertionConsumerServiceURL>
             <SignResponse>true</SignResponse>
             <EnableAudienceRestriction>true</EnableAudienceRestriction>
             <EnableRecipients>true</EnableRecipients>
             <AudiencesList>
                <Audience>https://localhost:9443/oauth2/token</Audience>
             </AudiencesList>
             <RecipientList>
                <Recipient>https://localhost:9443/oauth2/token</Recipient>
             </RecipientList>
          </ServiceProvider>
          <ServiceProvider>
             <Issuer>analyticsportal</Issuer>
             <AssertionConsumerServiceURLs>
                <AssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/portal/acs</AssertionConsumerServiceURL>
             </AssertionConsumerServiceURLs>
             <DefaultAssertionConsumerServiceURL>https://mgt.iots310.wso2.com:443/portal/acs</DefaultAssertionConsumerServiceURL>
             <SignResponse>true</SignResponse>
             <EnableAudienceRestriction>true</EnableAudienceRestriction>
             <EnableRecipients>true</EnableRecipients>
             <AudiencesList>
                <Audience>https://localhost:9443/oauth2/token</Audience>
             </AudiencesList>
             <RecipientList>
                <Recipient>https://localhost:9443/oauth2/token</Recipient>
             </RecipientList>
          </ServiceProvider>
       </ServiceProviders>
    </SSOIdentityProviderConfig>


4.  Start the Entgra IoT Server's core profile.

    
    cd <IOTS_HOME>/bin
    iot-server.sh
    
    
# Configuring the Manager Node


Throughout this guide, you have configured `mgt.iots.wso2.com` as the manager node.



Before you begin



*   Mount the registry as explained [here](/doc/en/lb2/Mounting-the-Registry.html).
*   Configure the following databases for the Key Manager in the `<IOTS_HOME>/conf/datasources/master-datasources.xml` file.  
    For more information, see [Setting Up the Databases for Clustering](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/clusturing-entgra-iot-server/#setting-up-the-databases-for-clustering).
    *   Registry database
    *   User manager database
    *   APIM database
    *   App manager database and include the social and storage database schemas to the same database.
    *   CDM database and include the certificate management, android, iOS and windows database schemas to the same database.


Let's start configuring the Manager node.

1.  Configure the `HostName` and  `MgtHostName` properties in the `<IOTS_HOME>/conf/carbon.xml` file as shown below.

    
    <HostName>iots.wso2.com</HostName>
    <MgtHostName>mgt.iots.wso2.com</MgtHostName>
    

    Make sure to have the `Offset` property configured to zero. If it is set to a value other than zero, you need to update the NGINX configuration based on the port offset.

2.  Configure the `<IOTS_HOME>/bin/iotserver.sh` file as shown below:

    -Diot.analytics.host="analytics.iots.wso2.com" \
    -Diot.analytics.http.port="80" \
    -Diot.analytics.https.port="443" \
    -Diot.analytics.thrift.port="7613" \
    -Diot.manager.host="mgt.iots.wso2.com" \
    -Diot.manager.https.port="443" \
    -Diot.core.host="iots.wso2.com" \
    -Diot.core.https.port="443" \
    -Diot.keymanager.host="keymgt.iots.wso2.com" \
    -Diot.keymanager.https.port="443" \
    -Diot.gateway.host="gateway.iots.wso2.com" \
    -Diot.gateway.https.port="443" \
    -Diot.gateway.http.port="80" \
    -Diot.gateway.carbon.https.port="443" \
    -Diot.gateway.carbon.http.port="80" \
    -Diot.apimpublisher.host="gateway.iots.wso2.com" \
    -Diot.apimpublisher.https.port="443" \
    -Diot.apimstore.host="gateway.iots.wso2.com" \
    -Diot.apimstore.https.port="443" \
    

3.  The publisher and store of the app manager run on manager node. Configuring the app manager:

    1.  Configure the following properties in the  `<IOTS_HOME>/repository/deployment/server/jaggeryapps/store/config/store.json` file for SSO by replacing  `https://localhost:9443` with `https://keymgt.iots.wso2.com`.

        *   `identityProviderURL`

        *   `storeAcs`

        js
        "ssoConfiguration": {
          "enabled": true,
          "issuer": "store",
          "identityProviderURL": "https://keymgt.iots.wso2.com/samlsso",
          "keyStorePassword": "wso2carbon",
          "identityAlias": "wso2carbon",
          "responseSigningEnabled": "true",
          "storeAcs": "https://mgt.iots.wso2.com/store/acs",
          "keyStoreName": "/repository/resources/security/wso2carbon.jks",
          "validateAssertionValidityPeriod": true,
          "validateAudienceRestriction": true,
          "assertionSigningEnabled": true
        },
        

    2.  Configure the following properties in the  `<IOTS_HOME>/repository/deployment/server/jaggeryapps/publisher/config/publisher.json` file for SSO by replacing  [`https://localhost:9443`](https://localhost:9443/)  with  [`https://keymgt.iots310.wso2.com`](https://keymgt.iots310.wso2.com/) .

        *   `identityProviderURL`

        *   `publishereAcs`

        js
        "ssoConfiguration": {
          "enabled": true,
          "issuer": "publisher",
          "identityProviderURL": "https://keymgt.iots.wso2.com/samlsso",
          "keyStorePassword": "wso2carbon",
          "identityAlias": "wso2carbon",
          "responseSigningEnabled": "true",
          "publisherAcs": "https://mgt.iots.wso2.com/publisher/sso",
          "keyStoreName": "/repository/resources/security/wso2carbon.jks",
          "validateAssertionValidityPeriod": true,
          "validateAudienceRestriction": true,
          "assertionSigningEnabled": true
        }
        

    3.  Configure the `AppDownloadURLHost` property in the `<IOTS_HOME>/conf/app-manager.xml` to point to  `http://mgt.iots.wso2.com` .

        `<Config name="AppDownloadURLHost">http://mgt.iots.wso2.com</Config>`

4.  Configure the following properties in the  `<IOTS_HOME>/repository/deployment/server/jaggeryapps/api-store/site/conf/site.json` file for SSO by replacing  [`https://localhost:9443`](https://localhost:9443/)  with  [`https://keymgt.iots310.wso2.com`](https://keymgt.iots310.wso2.com/) .

    js
    "ssoConfiguration": {
      "enabled": "true",
      "issuer": "API_STORE",
      "identityProviderURL": "https://keymgt.iots.wso2.com/samlsso",
      "keyStorePassword": "",
      "identityAlias": "",
      "responseSigningEnabled": "true",
      "assertionSigningEnabled": "true",
      "keyStoreName": "",
      "passive": "false",
      "signRequests": "true",
      "assertionEncryptionEnabled": "false"
    },
    

5.  Configure the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/android-web-agent/app/conf/config.json` file to update the Android agent download URL.

    js
    "generalConfig": {
      "host": "https://mgt.iots.wso2.com",
      "companyName": "WSO2 IoT Server",
      "browserTitle": "WSO2 IoT Server",
      "copyrightText": "\u00A9 %date-year%, WSO2 Inc. (http://www.wso2.org) All Rights Reserved."
    },
    

6.  Configure the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/devicemgt/app/conf/config.json` file to update the URL of the QR code, which will be used to enroll a device by scanning the QR code.

    js
    "generalConfig": {
      "host": "https://mgt.iots.wso2.com",
      "companyName": "WSO2 Carbon Device Manager",
      "browserTitle": "WSO2 Device Manager",
      "copyrightPrefix": "\u00A9 %date-year%, ",
      "copyrightOwner": "WSO2 Inc.",
      "copyrightOwnersSite": "http://www.wso2.org",
      "copyrightSuffix": " All Rights Reserved."
    },
    

7.  Start the core profile of WSO2 IoT Server.

    
    cd <IOTS_HOME>/bin
    ./iot-server.sh
    

8.  Optionally, enable the device status monitoring task on the manager node and disable it on the other nodes. Open the `<IOTS_HOME>/conf/cdm-config.xml` file and make sure the `DeviceStatusTaskConfig` is enabled. This configuration is enabled by default. For more information, see [Monitoring the Device Status](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/Monitoring-the-Device-Status/).


    In a clustered environment make sure to enable this task only in the manager node and not the worker nodes. Else, the server crashes when the worker nodes start pushing notifications along with the manager node. 

9.  Optionally, open the `<IOTS_HOME>/conf/cdm-config.xml` file and make sure the `SchedulerTaskEnabled` that is under `PushNotificationConfiguration` is enabled. This configuration is enabled by default. For more information, see [Scheduling the Push Notification Task](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/Scheduling-the-Push-Notification-Task/).


    In a clustered environment make sure to enable this task only in the manager node and not the worker nodes. Else, the server crashes when the worker nodes start pushing notifications along with the manager node. 

    
# Clustering the iOS Server


Let's take a look at the steps you need to follow to cluster Entgra IoT Server with iOS. This section is not required if you don't want to enroll and manage iOS devices.



Before you begin



Make sure to [install the iOS features.](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/working-with-ios/ios-configurations/)






Follow the steps given below:

1.  Run the following scripts in the given order to create the CA, RA and SSL certificates.

    

    

    Make sure to create the `output` folder and use the NGINX private key (`iots310_wso2_com.key`) as the CA.

    **NOTE**: Run the scripts only on one server and copy them to the other servers.

    

    

    
    SSL_PASS="wso2carbon"
    CA_SUBJ="/C=SL/ST=Western/L=Colombo/O=WSO2/OU=CDM/CN=*.iots310.wso2.com/EMAILADDRESS=noreply@wso2.com"
    RA_SUBJ="/C=SL/ST=Western/L=Colombo/O=WSO2/OU=CDM/CN=iots310.wso2.com/EMAILADDRESS=noreply@wso2.com"
    SSL_SUBJ="/C=SL/ST=Western/L=Colombo/O=WSO2/OU=CDM/CN="$1

    ------------------------------------------------------------------------------------------
    echo "Generating CA"
    openssl req -new -key ./output/iots310_wso2_com.key -out ./output/ca.csr -subj $CA_SUBJ
    openssl x509 -req -days 365 -in ./output/ca.csr -signkey ./output/iots310_wso2_com.key -out ./output/ca.crt -extensions v3_ca -extfile ./needed_files/openssl.cnf
    openssl rsa -in ./output/iots310_wso2_com.key -text > ./output/ca_private.pem
    openssl x509 -in ./output/ca.crt -out ./output/ca_cert.pem

    ------------------------------------------------------------------------------------------ 
    echo "Generating RA"
    openssl genrsa -out ./output/ra_private.key 4096
    openssl req -new -key ./output/ra_private.key -out ./output/ra.csr -subj $RA_SUBJ
    openssl x509 -req -days 365 -in ./output/ra.csr -CA ./output/ca.crt -CAkey ./output/iots310_wso2_com.key -set_serial 12132121241241 -out ./output/ra.crt -extensions v3_req -extfile ./needed_files/openssl.cnf
    openssl rsa -in ./output/ra_private.key -text > ./output/ra_private.pem
    openssl x509 -in ./output/ra.crt -out ./output/ra_cert.pem

    echo "Generating SSL"
    openssl genrsa -out ./output/ia.key 4096
    openssl req -new -key ./output/ia.key -out ./output/ia.csr  -subj $SSL_SUBJ
    openssl x509 -req -days 730 -in ./output/ia.csr -CA ./output/ca_cert.pem -CAkey ./output/ca_private.pem -set_serial 34467867966445 -out ./output/ia.crt

    ------------------------------------------------------------------------------------------ 
    echo "Export to PKCS12"
    openssl pkcs12 -export -out ./output/KEYSTORE.p12 -inkey ./output/ia.key -in ./output/ia.crt -CAfile ./output/ca_cert.pem -name "ioscluster" -password pass:$SSL_PASS
    openssl pkcs12 -export -out ./output/ca.p12 -inkey ./output/ca_private.pem -in ./output/ca_cert.pem -name "cacert" -password pass:$SSL_PASS
    openssl pkcs12 -export -out ./output/ra.p12 -inkey ./output/ra_private.pem -in ./output/ra_cert.pem -chain -CAfile ./output/ca_cert.pem -name "racert" -password pass:$SSL_PASS

    ------------------------------------------------------------------------------------------
    echo "Export PKCS12 to JKS"
    keytool -importkeystore -srckeystore ./output/KEYSTORE.p12 -srcstoretype PKCS12 -destkeystore ./output/wso2carbon.jks -deststorepass wso2carbon -srcstorepass wso2carbon -noprompt
    keytool -importkeystore -srckeystore ./output/KEYSTORE.p12 -srcstoretype PKCS12 -destkeystore ./output/client-truststore.jks -deststorepass wso2carbon -srcstorepass wso2carbon -noprompt
    keytool -importkeystore -srckeystore ./output/ca.p12 -srcstoretype PKCS12 -destkeystore ./output/wso2certs.jks -deststorepass wso2carbon -srcstorepass wso2carbon -noprompt
    keytool -importkeystore -srckeystore ./output/ra.p12 -srcstoretype PKCS12 -destkeystore ./output/wso2certs.jks -deststorepass wso2carbon -srcstorepass wso2carbon -noprompt
    

2.  Configure the `<IOTS_HOME>/conf/iot-api-config.xml` file in the manager node as shown below:

    
    <VerificationEndpoint>https://iots310.wso2.com/api/certificate-mgt/v1.0/admin/certificates/verify/</VerificationEndpoint>
    <DynamicClientRegistrationEndpoint>https://keymgt.iots310.wso2.com/client-registration/v0.11/register</DynamicClientRegistrationEndpoint>
    <OauthTokenEndpoint>https://gateway.iots310.wso2.com/token</OauthTokenEndpoint>
    

3.  Configure the `<IOTS _HOME>/conf/certificate-config.xml` file as shown below: 

    

    

    `wso2carbon` was used as the password when generating the CA and RA certificates using the scripts given in step 1\. If you used a different password, make sure to update the properties given below accordingly.

    

    

    
    <CAPrivateKeyPassword>wso2carbon</CAPrivateKeyPassword>
    <RAPrivateKeyPassword>wso2carbon</RAPrivateKeyPassword>
    

4.  Disable the task server in the `<IOTS_HOME>/repository/deployment/server/devicetypes/ios.xml` file on the worker node.

    
    <TaskConfiguration>
        <Enable>false</Enable>
        ......
    </TaskConfiguration>
    

5.  Configure the following properties in the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/ios-web-agent/app/conf/config.json` file as shown below.

    js
    "httpsURL": "https://mgt.iots310.wso2.com",
    "httpURL": "http://mgt.iots310.wso2.com",
    "tokenServiceURL": "https://gateway.iots310.wso2.com/token"
    "location": "https://mgt.iots310.wso2.com/ios-web-agent/public/mdm.page.enrollments.ios.download-agent/asset/ios-agent.ipa",
    

6.  Start the manager node, Sign in to the device management console, and navigate to the platform configurations section to configure the iOS configurations.  

    1.  Make use you have the MDM certificate and the MDM APNS certificate before you configure the iOS configurations. For more information, see [Generating Certificates from the Apple Developer Portal](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/working-with-ios/ios-configurations/#generating-certificates-from-the-apple-developer-portal).
    2.  Configure the iOS platform settings. For more information, see [iOS Platform Configurations](https://entgra-documentation.gitlab.io/v3.7.0/docs/Working-with-Android-Devices/Working-with-Android-Devices.html).



# Clustering WSO2 DAS for Analytics

This section guides you on how to cluster the WSO2 Data Analytics Server (WSO2 DAS) with Entgra IoT Server. **The minimum high availability deployment scenario is usually sufficient to cluster DAS but it depends on your use case**. 

Configure the `<IOTS_HOME>/conf/etc/device-analytics-config.xml file in both IoT Manager and IoT Worker nodes to configure device data publishing for Analytics cluster. Below sample configuration shows how to configure DAS cluster with 2 worker nodes (wss://10.0.10.20:9445,wss://10.0.10.21:9445) and 1 failover node (wss://10.0.10.30:9445). Please note that these configurations are internal configs and hence only needed to configure with private IPs.`

**device-analytics-config.xml**


<AnalyticsConfiguration>
    <Enabled>true</Enabled>
    <!--
        Server URL of the remote DAS/BAM/CEP server used to collect statistics. Must
        be specified in protocol://hostname:port/ format.

        An event can also be published to multiple Receiver Groups each having 1 or more receivers. Receiver
        Groups are delimited by curly braces whereas receivers are delimited by commas.
        Ex - Multiple Receivers within a single group
            tcp://localhost:7612/,tcp://localhost:7613/,tcp://localhost:7614/
        Ex - Multiple Receiver Groups with two receivers each
            {tcp://localhost:7612/,tcp://localhost:7613},{tcp://localhost:7712/,tcp://localhost:7713/}
    -->
    <ReceiverServerUrl>{tcp://10.0.10.20:7613|tcp://10.0.10.30:7613},{tcp://10.0.10.21:7613|tcp://10.0.10.30:7613}</ReceiverServerUrl>
    <!--
        Server URL of the remote DAS/BAM/CEP server used to subscribe for statistics via secured web sockets.
        Must be specified in wss://hostname:port/ format. Analytics Publishers should defined per each receiver
        server url.

        Multiple AnalyticsPublisherUrl properties can be defined as Groups each having one or more publishers.
        Publisher groups are delimited by curly braces whereas publishers are delimited by commas.
        Ex - Multiple publishers within a single group
            wss://localhost:9445/,wss://localhost:9446/,wss://localhost:9447/
        Ex - Multiple Publisher Groups with two publishers each
            {wss://localhost:9445/,wss://localhost:9446/},{wss://localhost:9447/,wss://localhost:9448/}
    -->
    <AnalyticsPublisherUrl>wss://10.0.10.20:9445,wss://10.0.10.21:9445,wss://10.0.10.30:9445</AnalyticsPublisherUrl>
    <AdminUsername>admin</AdminUsername>
    <AdminPassword>admin</AdminPassword>
</AnalyticsConfiguration>


